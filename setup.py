from setuptools import find_packages, setup

setup(
    name='couDAS',
    packages=find_packages(include=['couDAS']),
    version='0.1.0',
    description='A Python library for Data Analysis and Data Science',
    author='Thibault de Courson',
    author_email='thibault.db.decourson@gmail.com',
    install_requires=[],
    setup_requires=['pytest-runner'],
    tests_require=['pytest==4.4.1'],
    test_suite='tests',
    keywords=['python', 'data analysis', 'data science'],
)