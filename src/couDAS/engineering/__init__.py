from datetime import timedelta, date, time, datetime
import shutil
from os import listdir, unlink, makedirs
from os.path import isdir, join, isfile, islink
import pandas as pd
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import pickle


def reshape_datetime(t, mode='simplify'):
    """ Operates transformations on a datetime-like objects
    @param t: datetime-like object to process
    @param mode: type of operation to perform
    @return: object transformed

    @author Thibault de Courson, 2022
    """

    if mode == 'simplify':
        return t if t is None or type(t) is date or t.time() != time() else t.date()
    elif mode == 'datetime':  # returns full datetime
        return datetime.combine(t, datetime.min.time()) if type(t) is date else t
    elif mode == 'date':
        return t.date() if type(t) is datetime else t
    else:
        raise ValueError('Unknown reshape_datetime() mode')


def write_datetime(t, t2=None, time_range=False, verbose=False):
    """ Write a time range as an easily readable object
    @param t: left bound of the time range (included)
    @param t2: right bound of the time range (not included)
    @param time_range: generate a time range
    @param verbose: use words (from, between...) rather than a dash to separate dates
    @return: a string describing the time range

    @author Thibault de Courson, 2022
    """

    if type(t) == type(t2) and t2 - timedelta(days=1) == t: t2 = None
    separator = ' to ' if verbose else ' - '
    if time_range:
        # Naming an interval

        if t2 is None:
            # Naming a 24h interval

            name = write_datetime(t)
            if verbose: name = 'during ' + name

        elif type(t) is date and type(t2) is date:
            # Naming a date interval

            if t.day == 1 and t2.day == 1:
                # Naming a month or year interval

                if t.month == 1 and t2.month == 1:
                    # Naming a year interval

                    name = str(t.year)
                    if t2.year > t.year + 1:
                        # Naming a multiple years interval
                        name = str(t.year) + separator + str(t2.year - 1)
                    elif verbose:
                        # Naming a one-year interval
                        name = 'during ' + name
                else:
                    # Naming a month interval

                    temp = t2
                    while temp.month == t2.month:
                        t2 -= timedelta(days=1)

                    name = str(t.year) + ' ' + t.strftime("%B")
                    if t.year == t2.year:
                        # Naming a month interval on one year

                        if t.month == t2.month:
                            # Naming a one-month interval
                            if verbose:
                                name = 'during ' + name
                        else:
                            # Naming a multiple months interval
                            name += separator + t2.strftime("%B")
                    else:
                        # Naming a month interval on multiple years
                        name = str(t.year) + ' ' + t.strftime("%B") + separator + str(t2.year) + ' ' + t2.strftime("%B")
            else:
                t2 -= timedelta(days=1)

                if t.year == t2.year:
                    if t.month == t2.month:
                        # Naming a day interval on one month
                        name = str(t.year) + ' ' + t.strftime("%B") + ' ' + str(t.day) + separator + str(t2.day)
                    else:
                        # Naming a day interval on one year
                        name = str(t.year) + ' ' + t.strftime("%B") + ' ' + str(t.day) + separator + \
                               t2.strftime("%B") + ' ' + str(t2.day)
                else:
                    # Naming a simple date interval (day interval on multiple months)
                    name = str(t.year) + ' ' + t.strftime("%B") + ' ' + str(t.day) + separator + str(t2.year) + ' ' + \
                           t2.strftime("%B") + ' ' + str(t2.day)

        else:

            t = reshape_datetime(t, mode='datetime')
            t2 = reshape_datetime(t2, mode='datetime')

            if t.date() == t2.date():
                # Naming a time interval on one day

                name = str(t.date()) + ' ' + str(t.hour)
                if t.second == 0 and t2.second == 0:
                    # Naming an hour or minutes interval

                    if t.minute == 0 and t2.minute == 0:
                        # Naming an hour interval

                        if t2.hour > t.hour + 1:
                            # Naming a multiple hours interval
                            name += separator + str(t2.hour)
                        elif verbose:
                            # Naming a one-hour interval
                            name = 'during ' + name

                    elif t.hour == t2.hour:
                        # Naming a minute interval on one hour

                        if t2.minute > t.minute + 1:
                            # Naming a multiple minutes interval
                            name += str(t.minute) + separator + str(t2.minute)
                        elif verbose:
                            # Naming a one-minute interval
                            name = 'during ' + name
                    else:
                        # Naming a minute interval on multiple hours
                        name += str(t.minute) + separator + str(t2.hour) + ' ' + str(t2.minute)

                elif t.minute == t2.minute and t.hour == t2.hour:
                    # Naming a second interval on one minute
                    name = str(t.minute) + ' ' + str(t.second) + separator + str((t2 - timedelta(seconds=1)).second)
                else:
                    # Naming a simple time interval (second interval on one day)
                    t2 -= timedelta(seconds=1)
                    name = str(t.hour) + ':' + str(t.minute) + ':' + str(t.second) + separator + str(
                        t2.hour) + ':' + str(
                        t2.minute) + ':' + str(t2.second)

            else:
                # Naming a simple interval (seconds interval on multiple days)
                name = write_datetime(t) + separator + write_datetime(t2)

    else:
        name = str(t.year) + ' ' + t.strftime("%B") + ' ' + str(t.day)
        if type(t) is datetime:
            if t.second != 0:
                name += ' ' + str(t.time())
            elif t.minute != 0:
                name += ' ' + str(t.hour) + ':' + str(t.minute)
            elif t.hour != 0:
                name += ' ' + str(t.hour)
    return name


def empty_dir(path):
    """ Removes all files and subdirectories into a directory
    @param path: directory path

    @author Thibault de Courson, 2021
    """

    for filename in listdir(path):
        file_path = join(path, filename)
        try:
            if isfile(file_path) or islink(file_path):
                unlink(file_path)
            elif isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))


# Data standardisation, dropping unnecessary columns
def preprocess_data(df, scale_path=None, scaler='Standard'):
    # scale
    if scaler == 'Standard':
        scaler = StandardScaler()
    else:
        scaler = MinMaxScaler()
    scaled_cont_data = scaler.fit_transform(df)

    # save scaler for scaling of new data
    if scale_path is not None:
        pickle.dump(scaler, open(scale_path + '.pyc', 'wb'))

    # put in new dataframe
    return pd.DataFrame(scaled_cont_data, columns=df.columns)


def measure_duration(start, end, name=''):
    duration = (end - start).total_seconds()
    if name != '':
        print(name, ': ', sep='', end='')

    if duration < 60:
        print(round(duration, 2), "sec")
    else:
        print(round(duration / 60, 1), "min")


def initialize_dir(path, overwrite=False, create=True, warning=True):
    """ Initialize an empty directory

    @param path: path of the directory to initialize
    @param overwrite: if the directory already exists, empty it
    @param create: if the directory does not exist, create it
    @param warning: display warning
    @return: True if the operation was carried out without error

    @author Thibault de Courson, 2021
    """

    if isdir(path):
        # If the directory already exists

        if overwrite:
            # Empty the directory already created
            if warning:
                print('Folder', path, 'overwritten')
            empty_dir(path)

        else:
            # Do nothing
            if warning:
                print('Folder', path, 'already exists')
            return False

    elif create:
        # If the directory does not exist, create it
        makedirs(path)
    return True