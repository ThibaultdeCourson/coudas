def thorough_replace(text, to_catch, replacement):
    """
    Multiple Replace operations of a string

    :param text: string to process
    :param to_catch: Substring to search for
    :param replacement: substring to substitute
    :return: processed string
    """

    while text.find(to_catch) != -1:
        text = text.replace(to_catch, replacement)
    return text


def remove_trailing(text):
    """
    Remove abnormal trailing characters

    :param text: string to process
    :return: processed string
    """

    characters = {' ', '-', '_'}
    while len(text) > 1 and text[0] in characters:
        text = text[1:]
    while len(text) > 1 and text[len(text) - 1:] in characters:
        text = text[:len(text) - 1]
    return text
