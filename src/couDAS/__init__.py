def measure_duration(start, end, name=''):
    duration = end - start
    if name != '':
        print(name, ': ', sep='', end='')

    if duration < 60:
        print(round(duration, 2), "sec\n")
    else:
        print(round(duration / 60, 1), "min\n")


def octet_rectifier(x):
    if x < 0:
        return 0
    elif x > 255:
        return 255
    else:
        return round(x)


# Legacy

def simplify_angle(x):
    if x < 0:
        return round(179 + x)
    elif x > 179:
        return round(x - 179)
    else:
        return round(x)


def complete_dict(dictionary, complement, parameter):
    if type(complement[parameter]) == dict:
        for value in complement[parameter]:
            if parameter not in dictionary:
                dictionary[parameter] = {}

            dictionary[parameter][value] = complete_dict(dictionary[parameter], complement[parameter], value)
        return dictionary[parameter]
    else:
        return complement[parameter]
