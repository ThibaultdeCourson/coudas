import cv2 as cv
import numpy as np
from math import sqrt, cos, sin, atan, tan
from matplotlib.colors import to_rgb


def compare_position(A, B, order, anchor='border'):
    if anchor == 'border':
        if order == 'left':
            return A['left'] < B['left']
        elif order == 'right':
            return A['right'] > B['right']
        elif order == 'top':
            return A['top'] < B['top']
        else:
            return A['bottom'] > B['bottom']
    else:
        if order == 'left':
            return A['xc'] < B['left']
        elif order == 'right':
            return A['xc'] > B['right']
        elif order == 'top':
            return A['yc'] < B['top']
        else:
            return A['yc'] > B['bottom']


def sort_list(dict_list, order):
    if len(dict_list) > 1:
        if order == 'left':
            dict_list = sorted(dict_list, key=lambda element: element['xc'])
        elif order == 'right':
            dict_list = sorted(dict_list, key=lambda element: element['xc'], reverse=True)
        elif order == 'top':
            dict_list = sorted(dict_list, key=lambda element: element['yc'])
        else:
            dict_list = sorted(dict_list, key=lambda element: element['yc'], reverse=True)
    return dict_list


def split_channels(img):
    color_spaces = np.vstack(cv.split(img))
    color_space_names = []
    for name, conversion in {'HSV': (cv.COLOR_BGR2HSV, (255 / 179, 1, 1)),
                             'LAB': (cv.COLOR_BGR2LAB, (1, 1, 1)),
                             'YCrCb': (cv.COLOR_BGR2YCrCb, (1, 1, 1))}.items():
        color_space_names.append(name)
        img_conv = cv.cvtColor(img, conversion[0])
        for channel in range(3):
            img_conv[:, :, channel] = np.round_(img_conv[:, :, channel] * conversion[1][channel])
        color_spaces = np.hstack((color_spaces, np.vstack(cv.split(img_conv))))

    display(color_spaces, "Color spaces: BGR," + ", ".join(color_space_names))


def resize_pic(img, w):
    scale_percent = 100 * w / img.shape[1]
    dim = (int(img.shape[1] * scale_percent / 100), int(img.shape[0] * scale_percent / 100))

    resized = cv.resize(img, dim, interpolation=cv.INTER_AREA)
    return resized


def crop_top_img(image, p):  # removes the top of the image (p=0.2 removes 20%)
    s = int(image.shape[0] * p)
    mask = np.zeros((image.shape[0], image.shape[1]), dtype=np.uint8)
    for i in range(s, image.shape[0]):
        for j in range(0, image.shape[1]):
            mask[i, j] = 255
    crop = cv.bitwise_and(image, image, mask=mask)
    # cv.imshow("m",crop)
    # cv.waitKey()
    return crop


def to_opencv_hsv(color, inv=False):
    if inv:
        return [color[0] / 179 * 360, color[1] / 255 * 100, color[2] / 255 * 100]
    else:
        return [color[0] * 179 / 360, color[1] * 255 / 100, color[2] * 255 / 100]


# def to_opencv_hsv(hue, saturation, value, inv=False):
#     if inv: return [hue / 179 * 360, saturation / 255 * 100, value / 255 * 100]
#     else: return [hue * 179 / 360, saturation * 255 / 100, value * 255 / 100]


def filter_colors(img, lower_color, upper_color, verbose=False):
    img = cv.cvtColor(img, cv.COLOR_BGR2HSV)

    if lower_color[0] > upper_color[0]:
        if verbose:
            print("0-", round(upper_color[0] * 360 / 179), "&", round(lower_color[0] * 360 / 179), "-360, ",
                  round(lower_color[1] * 100 / 255), "-", round(upper_color[1] * 100 / 255), ", ",
                  round(lower_color[2] * 100 / 255), "-", round(upper_color[2] * 100 / 255), sep='')
            color_range = upper_color[0] + 179 - lower_color[0]
            print("Color range: ", round(color_range * 100 / 179, 1),
                  "% hue, ", round(color_range * (upper_color[1] - lower_color[1]) * (upper_color[2] - lower_color[2])
                                   * 100 / (179 * 255 * 255), 1), "% color", sep='')

        return cv.bitwise_or(cv.inRange(img, np.array([0, lower_color[1], lower_color[2]]),
                                        np.array(upper_color)),
                             cv.inRange(img, np.array(lower_color),
                                        np.array([179, upper_color[1], upper_color[2]])))
    else:
        if verbose:
            print(round(lower_color[0] * 360 / 179), round(upper_color[0] * 360 / 179),
                  round(lower_color[1] * 100 / 255), round(upper_color[1] * 100 / 255),
                  round(lower_color[2] * 100 / 255), round(upper_color[2] * 100 / 255))
            print("Color range: ", round((upper_color[0] - lower_color[0]) * 100 / 179, 1), "% hue, ",
                  round((upper_color[0] - lower_color[0]) * (upper_color[1] - lower_color[1]) *
                        (upper_color[2] - lower_color[2]) * 100 / (179 * 255 * 255), 1), "% color", sep='')
        return cv.inRange(img, np.array(lower_color), np.array(upper_color))


def get_distance(a, b):
    return sqrt((b[0] - a[0]) ** 2 + (b[1] - a[1]) ** 2)


def point_in_rects(point, rectangles, exclude_rectangle):
    for bBox in [rectangles.copy().pop(exclude_rectangle)]:
        if point_in_rect(point, bBox):
            return False
    return True


def point_in_rect(point, rectangle):
    return rectangle['left'] < point[0] < rectangle['right'] and rectangle['top'] < point[1] < rectangle['bottom']


def get_point(x, y, theta, width, height):
    theta *= np.pi / 180  # theta en radian

    if -atan((height - y) / x) <= (theta if theta <= np.pi else theta - 2 * np.pi) <= atan(y / x) \
            or np.pi - atan(y / (width - x)) <= theta <= np.pi + atan((height - y) / (width - x)):
        # Left and right sides
        # print("Left and right sides, inversion =", cos(theta) < 0)
        base = -x if cos(theta) > 0 else width - x - 1
        return x + base, y + round(base * tan(theta))
    else:
        # Top and bottom sides
        # print("Top and bottom sides, inversion =", sin(theta) < 0)
        base = -y if sin(theta) > 0 else height - y - 1
        return x + round(base / tan(theta)) if theta % np.pi / 2 != 0 else base, y + base


def compute_shape1(point, theta, size=(1080, 1920)):
    print("\n\npoint", point, ", Angle", theta, "° =", theta * np.pi / 180, "rad, Size", size[1], size[0])
    theta *= np.pi / 180  # theta en radian

    return get_point(point[0], point[1], theta, size[1], size[0]), \
        get_point(point[0], point[1], theta + np.pi, size[1], size[0])


def shift_coords(data, shifting, inverse=False):
    if len(data) > 0:
        if inverse:
            shifting = [-i for i in shifting]

        if isinstance(data[0], np.ndarray) or isinstance(data[0], list) or isinstance(data[0], tuple):
            if isinstance(data[0][0], np.ndarray) or isinstance(data[0][0], list or isinstance(data[0][0], tuple)):
                return [
                    np.array([np.array([np.array([coord[0] + shifting[0], coord[1] + shifting[1]]) for coord in plist])
                              for plist in array]) for array in data]
            else:
                return [[coord[0] + shifting[0], coord[1] + shifting[1]] for coord in data]
        else:
            return data[0] + shifting[0], data[1] + shifting[1]
    else:
        return []


def to_bgr(color):
    (red, green, blue) = to_rgb(color)
    return blue * 255, green * 255, red * 255


def display(img, name="img"):
    cv.imshow(name, img)
    cv.waitKey()


def enlarge(base_rect, rect_to_include, shifting=False):
    if shifting:
        rect_to_include = {'left': rect_to_include['left'] + base_rect['shifting'][0],
                           'right': rect_to_include['right'] + base_rect['shifting'][0],
                           'top': rect_to_include['top'] + base_rect['shifting'][1],
                           'bottom': rect_to_include['bottom'] + base_rect['shifting'][1]}
    if 'left' not in base_rect:
        base_rect['left'] = rect_to_include['left']
    elif rect_to_include['left'] < base_rect['left']:
        base_rect['left'] = rect_to_include['left']
    if 'right' not in base_rect:
        base_rect['right'] = rect_to_include['right']
    elif rect_to_include['right'] > base_rect['right']:
        base_rect['right'] = rect_to_include['right']
    if 'top' not in base_rect:
        base_rect['top'] = rect_to_include['top']
    elif rect_to_include['top'] < base_rect['top']:
        base_rect['top'] = rect_to_include['top']
    if 'bottom' not in base_rect:
        base_rect['bottom'] = rect_to_include['bottom']
    elif rect_to_include['bottom'] > base_rect['bottom']:
        base_rect['bottom'] = rect_to_include['bottom']


def crop_image(img, bBoxes, padding=0, padding_type='rel', shifting=(0, 0)):
    im = img.copy()
    CA_mask = np.zeros(img.shape[:2], dtype=np.uint8)

    if len(bBoxes) > 0:
        for bBox in bBoxes:

            if padding != 0:
                if padding_type == 'rel':
                    bBox['left'] -= int(padding * bBox['w']) + shifting[0]
                    bBox['top'] -= int(padding * bBox['h']) + shifting[1]
                    bBox['right'] += int(padding * bBox['w']) - shifting[0]
                    bBox['bottom'] += int(padding * bBox['h']) - shifting[1]
                else:
                    bBox['left'] -= padding + shifting[0]
                    bBox['top'] -= padding + shifting[1]
                    bBox['right'] += padding - shifting[0]
                    bBox['bottom'] += padding - shifting[1]

            reframe(bBox, img.shape)

            cv.rectangle(CA_mask, (bBox['left'], bBox['top']), (bBox['right'], bBox['bottom']), 255, -1)

        im = cv.bitwise_and(im, im, mask=CA_mask)
        im[CA_mask == 0] = (255, 255, 255)

    return im


def resize_shape(shape, changes):
    for i in changes:
        if i in ('left', 'top'):
            shape[i] -= changes[i]
        else:
            shape[i] += changes[i]
    return shape


def reframe(box, bottom_right, top_left=(0, 0)):
    if box['left'] < top_left[1]:
        box['left'] = top_left[1]
    if box['top'] < top_left[0]:
        box['top'] = top_left[0]
    if box['right'] > bottom_right[1]:
        box['right'] = bottom_right[1]
    if box['bottom'] > bottom_right[0]:
        box['bottom'] = bottom_right[0]


def expand_box(box, expansion, padding):
    for i in ('left', 'top'):
        box[i] -= expansion
    for i in ('right', 'bottom'):
        box[i] += expansion

    # Checking the padding box
    for i in ('left', 'top', 'right', 'bottom'):
        if (i in ('left', 'top') and box[i] < padding[i]) or (i in ('right', 'bottom') and box[i] > padding[i]):
            box[i] = padding[i]

    return box
