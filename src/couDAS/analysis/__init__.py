import numpy as np
import pandas as pd
from math import isinf
from statsmodels.stats.outliers_influence import variance_inflation_factor
import statsmodels.formula.api as smf
from sklearn.model_selection import KFold
from sklearn.metrics import confusion_matrix, accuracy_score


def analyse_dataframe(df, threshold=10, percentages=True):
    """
    Generates an insight of the content of the dataframe (improvement of DataFrame.info())

    :param df: dataframe to inspect
    :param threshold: maximum number of categories to display for a column
    :param percentages: display values as percentage
    :return: summary dataframe

    @author Thibault de Courson, 2020
    """

    print(len(df.columns), "columns for", len(df), "rows")

    n = len(df)
    summary = pd.DataFrame(columns=['type', 'number of different values', 'NaN values', 'True values'])

    for i in df.columns:
        val_counts = df[i].value_counts(dropna=False)  # Counts the number of values

        if df.dtypes[i] == 'bool':
            true_val = df[i].sum()
            summary.loc[i] = [df.dtypes[i], '', '',
                              str(round(100 * true_val / n, 1)) + "%" if percentages else true_val]
        else:
            null_val = df[i].isnull().sum()
            summary.loc[i] = [df.dtypes[i], len(val_counts), (
                str(round(100 * null_val / n, 1)) + "%" if percentages else null_val) if null_val > 0 else '', '']

        if df.dtypes[i] is str or df.dtypes[i] == 'object':

            if percentages:
                val_counts = 100 * val_counts / n
                val_counts = val_counts.round(1).astype(str) + ' %'

            print()

            print("[", i, "] (", len(val_counts), " values)", sep='', end='')

            if len(val_counts) <= threshold:
                print(":\n", val_counts, sep='')  # List categories of a categorical column
            else:
                print()

    return summary


def fill_unknown(df, features, categories='add'):
    """
    Fill null values of a column

    :param df: dataframe
    :param features: name (or list of names) of the column(s)
    :param categories: choose how to deal with categorical columns
    :return: modified dataframe

    @author Thibault de Courson, 2022
    """

    if type(features) != list:
        features = [features]

    prop = 100 / df.shape[0]

    for feature in features:
        percentage = None

        missing_values = df[feature].isnull()
        print(round(missing_values.sum() * prop, 1), "% of values missing for the [", feature, "] variable", sep='',
              end='')

        default = None
        if df[feature].dtype == 'object':
            if categories == 'freq':
                value_counts = df[feature].value_counts(sort=True)
                default = value_counts.index[0]
                percentage = round(value_counts[0] * prop, 1)
            else:
                default = 'unknown'
        else:
            default = df[feature].median()

        print(". Replacement value choosen: ", default, sep='', end='')
        if percentage is not None:
            print(" (", percentage, "% of the original values)", sep='')
        else:
            print()

        df.loc[df[missing_values].index, feature] = default

    return df


def vif(data, threshold=10, labels=None, descriptions=None, return_df=None):
    """
    Remove highly correlated variables using the Variance Inflation Factor

    :param data: dataframe or array
    :param threshold: maximum VIF value to accept
    :param labels: labels of the variables (not necessary if a dataframe with labels is given)
    :param descriptions: description of the variables (if labels are not intuitive enough)
    :param return_df: force return a dataframe (by default returns the same type given as input)
    :return: the data (dataframe or array) and a dataframe of the VIF values of the retained variables
    """

    index = None
    df_type = isinstance(data, pd.DataFrame)
    if return_df is None:
        return_df = df_type

    if labels is None:
        if df_type:
            labels = pd.Series(data.columns)
        else:
            labels = pd.Series(list(range(data.shape[1])))

    if df_type:
        index = data.index
        data = data.values

    vif = [variance_inflation_factor(data[:, labels.index.values], i) for i in
           range(len(labels))]  # Compute the VIF of the selected parameters

    max_vif = max(vif)  # Select the gene with the worst VIF

    while not isinf(max_vif) and max_vif >= threshold:
        # Remove the predictor with the worst VIF
        predictor = labels.iloc[vif.index(max_vif)]
        print("Dropping [", descriptions[predictor] if descriptions is not None else predictor, "] (VIF of ",
              round(max_vif), ")", sep='')
        labels = labels[labels != predictor]

        vif = [variance_inflation_factor(data[:, labels.index.values], i) for i in
               range(len(labels))]  # Compute the VIF of the selected parameters
        max_vif = max(vif)  # Select the gene with the worst VIF

    if isinf(max_vif):
        print("VIF inoperable: returns infinite value")

    print(data.shape[1] - len(vif), "predictors removed,", len(labels), "remaining")

    data = data[:, labels.index.values]

    if return_df:
        data = pd.DataFrame(data, index=index, columns=labels)

    return data, pd.Series(vif, index=[descriptions[param] for param in labels] if descriptions is not None else labels,
                           name="VIF").sort_values(ascending=False)


def cross_validation_classifier(df, query, label_col='outcome', k_fold=5, cutoff=.5):
    """
    Performs n random trainings/tests to build a confusion matrix and prints results with precision and recall scores

    :param df: the dataset to work on
    :param query: a statsmodels query
    :param label_col: column containing the labels
    :param k_fold: number of folds
    :param cutoff: cutoff value
    :return: a dictionary containing the results
    """

    data = {
        "Accuracy": None,
        "Balanced Accuracy": None,
        "ROC AUC": [],
        "F1 Score": None,
        "Recall": None,
        "Confusion mat": None,
        "Model": None,
        "ROC": None,
        "ROCs": []
    }

    confusion_mat = np.zeros((2, 2))
    recall = 0
    Accuracy = 0
    B_Accuracy = 0
    F1 = 0
    combined_predictions = [[], []]
    kf = KFold(n_splits=k_fold, random_state=42, shuffle=True)

    # Apply k-fold cross-validation
    for train_ids, val_ids in kf.split(df):

        df_train = df.iloc[train_ids]
        df_val = df.iloc[val_ids]
        model = smf.logit(formula=query, data=df_train).fit()

        y_prob = model.predict(df_val)
        y_pred = pd.Series([1 if x > cutoff else 0 for x in y_prob], index=y_prob.index)

        combined_predictions[0].append(df_val[label_col])
        combined_predictions[1].append(y_prob)

        roc_auc = None
        try:
            # Compute the Receiver Operating Characteristic
            fpr, tpr, _ = roc_curve(df_val[label_col], y_prob)

            # Computes the Area Under the Curve
            roc_auc = auc(fpr, tpr)

            data['ROCs'].append((fpr, tpr, roc_auc))
            data['ROC AUC'].append(roc_auc)

        except Exception as exception:
            print("ROC AUC couldn't be calculated")
            print(exception)

        accs = pd.Series()
        for i in np.linspace(1 / 4, 3 / 4, 30):
            accs[i] = accuracy_score(df_val[label_col],
                                     pd.Series([1 if x > i else 0 for x in y_prob], index=y_prob.index), normalize=True)

        print("Cutoff value of ", round(accs.idxmax(), 2), " (Accuracy of ", round(100 * accs.max(), 1), ")", sep='')

        Accuracy += accuracy_score(df_val[label_col], y_pred, normalize=True)
        B_Accuracy += balanced_accuracy_score(df_val[label_col], y_pred)
        F1 += f1_score(df_val[label_col], y_pred, average="weighted")
        confusion_mat += confusion_matrix(df_val[label_col], y_pred)
        recall += recall_score(df_val[label_col], y_pred)

    # Generates an ROC Curve based on the predictions of all k-fold models
    try:
        combined_predictions[1] = np.concatenate(combined_predictions[1], axis=0)
        if combined_predictions[1].ndim == 2:
            combined_predictions[1] = combined_predictions[1][:, 1]
        fpr, tpr, _ = roc_curve(np.concatenate(combined_predictions[0]), combined_predictions[1])
        data['ROC'] = (fpr, tpr, auc(fpr, tpr))
    except Exception as exception:
        print("ROC AUC couldn't be calculated")
        print(exception)

    data["Accuracy"] = Accuracy / k_fold
    data["Balanced Accuracy"] = B_Accuracy / k_fold
    data["ROC AUC"] = mean(data["ROC AUC"])
    data["F1 Score"] = F1 / k_fold
    data["Recall"] = recall / k_fold
    data["Confusion mat"] = pd.DataFrame(confusion_mat, columns=['False', 'True'], index=['False', 'True'], dtype=int)
    data["Model"] = smf.logit(formula=query, data=df).fit()  # Build the final model on the whole dataset

    return data


def correlation_matrix(df, threshold=None, auto_corr=False, hide_dummies=None):
    """
    Generates a correlation matrix

    :param df: dataset
    :param threshold: Threshold of interest for correlation values (usefull to reduce the size of the matrix
    :param auto_corr: Whether the auto-correlation values should be hidden
    :param hide_dummies: List of families of dummies to hide
    :return: the correlation matrix
    """

    corr = df.corr()

    # Remove auto correlations
    if not auto_corr:
        for i in range(corr.shape[0]):
            corr.iat[i, i] = None

    # Remove columns and rows with low correlation maxima
    if threshold is not None or hide_dummies is not None:
        for dimension in corr.columns:
            col = corr[dimension]

            # Remove correlations between dummies of the same original categorical column
            if hide_dummies is not None:
                for category in hide_dummies:
                    if dimension.find(category + '_') == 0:
                        rows = col[col.index.str.find(category + '_') == 0].index
                        corr.loc[rows, dimension] = None
                        col.drop(rows, inplace=True)

            if threshold is not None:
                max_corr = col.abs().max()
                if max_corr < threshold:
                    print("Dropping low correlation dimension [", dimension, "] (", round(100 * max_corr, 1), "%)",
                          sep='')
                    del corr[dimension]
                    corr.drop(dimension, inplace=True)

        if threshold is not None:
            print(corr.shape[0], "dimensions kept\n")

    return corr


def bootstrap_validation(x_test, y_test, x_train, model, metrics, sample=500):
    metric_names = list(metrics.keys())
    metrics = list(metrics.values())
    n_metrics = len(metrics)
    # output_array = np.zeros(len(x_test))
    output_stats = np.zeros([sample, n_metrics])

    output_stats[:] = np.nan

    # Counting how many times that each datapoint has been classfied as winner overall
    for i_dataset in tqdm(range(sample)):

        # Generating subdataset
        if isinstance(x_test, np.ndarray):
            dataset_index = np.random.choice(x_test.shape[0], x_test.shape[0], replace=True)
            dataset = x_test[dataset_index, :]
        else:
            dataset_index = np.random.choice(x_test.index, len(x_test.index), replace=True)
            dataset = x_test.loc[dataset_index]
        labels = y_test[dataset_index]

        # Getting prediction values
        predicted_labels = model.predict(dataset)

        # output_array = output_array + predicted_labels

        for i_metric in range(n_metrics):
            output_stats[i_dataset, i_metric] = metrics[i_metric](predicted_labels, labels, x_train)

    # Counting how many successes in the test dataset
    # N = int(sum(y_test))
    # Select the top winners among entire bootstrapping examples, and return the rows of which data points will win
    # res = sorted(range(len(output_array)), key = lambda sub: output_array[sub])[-N:]

    return pd.DataFrame(output_stats, columns=metric_names)
