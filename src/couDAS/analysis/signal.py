import numpy as np
import pandas as pd
from scipy import signal
from datetime import timedelta
import plotly.graph_objects as go
from plotly.subplots import make_subplots


def streaming_edge_detection(x, sigma=2.0, threshold=3.0, abs_threshold=1, min_dist=5, visualize=False, title='x'):
    """
    streaming edge detection algorithm on signal x
    @param x: original time series
    @param sigma: sigma of Gaussian smooth filter, default=2.0
    @param threshold: threshold for edge detection. dx > ma * threshold, default=3.0
    @param abs_threshold:
    @param min_dist:
    @param visualize: generate a graph to illustrate
    @param title: title of thetitle graph (for visual representation)
    @return list of edge tuples [(sign, position)], e.g. [("+", 20), ("-", 30)]

    Source: https://notebook.community/KKTT/time-series-python-exp/edge-detection/streaming_edge_detection

    @author Thibault de Courson, 2022
    """

    width = int(3 * sigma)
    edges = []
    d1_gaussian = signal.convolve(in1=signal.gaussian(M=width * 2 + 1, std=sigma), in2=[-1, 0, 1], mode='same')
    # px.line(d1_gaussian).show()
    edge_pos = []
    edge_val = []
    last = -1

    derivative = np.zeros(len(x))
    dx_buffer = np.zeros(3)
    low_buffer = np.zeros(len(x))
    high_buffer = np.zeros(len(x))

    # convolve signal x with d1_gaussian window
    for n in range(width, len(x) - width):
        dx = 0
        if sigma > 0:
            # convolve sum first half
            for k in range(1, width + 1):
                dx += d1_gaussian[width - k] * x[n - k] if n >= k else 0

                # middle point
            dx += x[n] * d1_gaussian[width]

            # convolve sum second half
            for k in range(1, width + 1):
                dx += d1_gaussian[width + k] * x[n + k] if n + k < len(x) else 0
        else:
            # no smoothing at all
            dx = x[n] - x[n - 1] if n > 0 else 0

        derivative[n] = dx
        # buffer shift
        dx_buffer[0] = dx_buffer[1]
        dx_buffer[1] = dx_buffer[2]
        dx_buffer[2] = dx

        low_quantile = 0 if n == 0 else np.percentile(np.abs(derivative[:n]), 50)
        high_quantile = 0 if n == 0 else np.percentile(np.abs(derivative[:n]), 95)
        low_buffer[n] = low_quantile
        high_buffer[n] = high_quantile
        ratio = 0 if low_quantile == 0 else high_quantile / low_quantile
        sign = None

        if abs(dx_buffer.mean()) > abs_threshold:  # and not (dx == 0 and x[n] == 0):

            if dx_buffer[1] > low_quantile * threshold and dx_buffer[1] >= dx_buffer[2] and \
                    dx_buffer[1] >= dx_buffer[0] and n - 1 - last > min_dist:
                # find local max
                sign = '+'

            elif dx_buffer[1] < -low_quantile * threshold and dx_buffer[1] <= dx_buffer[2] and \
                    dx_buffer[1] <= dx_buffer[0] and n - 1 - last > min_dist:
                # find local min
                sign = '-'

        if sign is not None:
            position = x.index[n - 1]
            edges.append((sign, position, dx))
            edge_pos.append(position)
            edge_val.append(dx_buffer[1])
            last = n - 1

    if visualize:
        fig = make_subplots(rows=3, cols=1, shared_xaxes=True, vertical_spacing=0.05,
                            subplot_titles=[title,
                                            "Gaussian derivative signal and edge detector results",
                                            "quantile analysis"])
        fig.add_trace(go.Scatter(x=x.index, y=x), row=1, col=1)
        fig.add_trace(go.Scatter(x=x.index, y=derivative), row=2, col=1)
        fig.add_trace(go.Scatter(x=edge_pos, y=edge_val, mode='markers', name="detected edge positions"), row=2, col=1)
        fig.add_trace(go.Scatter(x=x.index, y=low_buffer, name="50th percentile"), row=3, col=1)
        fig.add_trace(go.Scatter(x=x.index, y=high_buffer, name="95th percentile"), row=3, col=1)
        fig.update_layout(height=800)
        fig.show()
    return edges, ratio


def square_wave_detector(x, sigma=2.0, threshold=3.0, abs_threshold=1, min_dist=5, visualize=False, title='x'):
    """
    @author Thibault de Courson, 2022
    """

    values, _ = streaming_edge_detection(x, sigma, threshold, abs_threshold, min_dist)
    mean = x.mean()
    impulses = []

    if len(values) > 1:
        origin = x.index[0]
        origin_type = False  # = void

        period = x[x.index[0]:values[0][1]].mean()
        if abs(period - mean) > abs_threshold:
            origin_type = True  # Record the detection of the start of a signal

        for i in range(1, len(values)):
            period = x[values[i - 1][1]:values[i][1]].mean()
            if abs(period - mean) > abs_threshold:
                if not origin_type:
                    origin = values[i - 1][1]  # Save the start time of the signal
                    origin_type = True  # Record the detection of the start of a signal

            else:
                if origin_type:
                    period = x[origin:values[i - 1][1]].mean()
                    # Add the complete signal to the list
                    impulses.append({'tmin': origin, 'tmax': values[i - 1][1],
                                     'sign': int(period / abs(period)), 'avg': period})

                    origin_type = False  # Record the detection of the absence of signal

        if origin_type:
            # If a complete signal was stored...
            period = x[origin:values[-1][1]].mean()
            impulses.append({'tmin': origin, 'tmax': values[-1][1], 'sign': int(period / abs(period)),
                             'avg': period})  # Add the complete signal to the list

    if visualize:
        print(len(impulses), "pulses detected")
        # pd.options.plotting.backend = "plotly"
        # df = pd.DataFrame({title: x, 'square wave': None})
        square = pd.Series()
        square[x.index[0]] = mean
        square[x.index[-1]] = mean
        for i in impulses:
            square[i['tmin'] - timedelta(seconds=1)] = mean
            square[i['tmin']] = i['avg']
            square[i['tmax']] = i['avg']
            square[i['tmax'] + timedelta(seconds=1)] = mean

        square = square.sort_index()

        # df['square wave'].loc[i['tmin'] : i['tmax']] = i['avg'] + mean

        fig = go.Figure(layout=dict(title=title + " square wave detection"))
        fig.add_trace(go.Scatter(x=x.index, y=x, line=dict(width=0.5), name='signal'))
        fig.add_trace(go.Scatter(x=square.index, y=square, line=dict(width=0.5), name='square wave'))
        fig.update_yaxes(title_text='Velocity (m/s)')
        fig.update_layout(template='plotly_dark')
        fig.show()

    return impulses


def SMM(x, delay=None, tolerance=0):
    """
    @author Thibault de Courson, 2022
    """

    if delay is None:
        return local_SMM(x, delay, tolerance)
    else:
        SMM_df = pd.Series(index=x.index, dtype='float')  # Initialize the series
        for t in x[x.index[0] + delay:].index:
            SMM_df[t] = local_SMM(x[t - delay:t], delay, tolerance)
        return SMM_df


def local_SMM(x, delay=None, tolerance=0):
    """
    @author Thibault de Courson, 2022
    """

    if delay is None:
        delay = timedelta(seconds=0)
    x = x.dropna()
    if x.empty:
        return 0
    else:
        local_min = min(x)
        local_max = max(x)
        sign = local_min == abs(local_min)

        if sign == (local_max == abs(local_max)) and len(x) >= delay.total_seconds() * tolerance:
            # If the whole series has the same sign (+/-) use an extremum as value
            return local_min if sign else local_max
        else:
            # If the whole series does not have the same sign (+/-) set its value to 0
            return 0


def zero_min(x):
    """
    @author Thibault de Courson, 2022
    """

    # Returns the minimum around zero from the array
    # If the values of the array have the same sign, return the value closest to zero
    # If the values of the array does not have the same sign, return zero
    if len(x) == 1:
        return x[0]
    else:
        tmin = np.min(x)
        tmax = np.max(x)
        return (tmin if tmin == abs(tmin) else tmax) if tmin * tmax > 0 else 0
