"""
Data visualization tools
"""

import numpy as np
import pandas as pd
from os.path import isfile
import plotly.graph_objects as go
from math import ceil, floor, isnan
from sklearn.neighbors import KernelDensity
import datetime as dt
from plotly.colors import DEFAULT_PLOTLY_COLORS
from plotly.subplots import make_subplots

from src.couDAS.engineering import initialize_dir

color = {'blue': '#636efa', 'orange': '#EF553B', 'green': '#00cc96'}


def histogram(fig, x, bandwidth=1, kde_step=None, unit='', color=None, auto_bin=False, position=None):
    """ Generates a Plotly histogram with a continuous line using Kernel Density Estimation

    @param fig: Plotly figure
    @param x: list or array
        Source data
    @param bandwidth: Float
        Bandwidth of the KDE
    @param kde_step: Float
        KDE sampling (for display)
    @param unit: string type
        Unit of the data studied (for display)
    @param color: list of two string or string
        Names of the colors of the bars and of the line
    @param auto_bin: boolean
        Let Plotly determine optimal bin size or use bandwidth
    @param position: list of two positive integers or None
        Where to add the graph in case of a subplots
    @return: Figure
        Completed figure

    @author Thibault de Courson, 2022
    """

    bar_marker = dict()
    line_marker = dict()
    x_bins = dict()
    if not auto_bin:
        x_bins['size'] = bandwidth
    if color is not None:
        if type(color) in (tuple, list) and len(color) > 1:
            bar_marker['color'] = color[0]
            line_marker['color'] = color[1]
        else:
            bar_marker['color'] = color

    if kde_step is None:
        kde_step = bandwidth / 5

    # Math based on: https://towardsdatascience.com/3-best-often-better-alternatives-to-histograms-61ddaec05305
    # Code based on: https://www.sefidian.com/2017/06/14/kernel-density-estimation-kde-in-python/
    # To study: https://stackabuse.com/kernel-density-estimation-in-python-using-scikit-learn/

    if type(x) == list:
        x = np.asarray([x for x in x if not isnan(x)])
    else:
        x = x[~(np.isnan(x))]  # Removes null values
        x = np.asarray(x)

    model = KernelDensity(bandwidth=bandwidth, kernel='gaussian').fit(
        x.reshape((len(x), 1)))  # Calculates Kernel Density Estimation
    values = np.asarray(np.arange(bandwidth * floor(min(x) / bandwidth) - bandwidth / 2,
                                  bandwidth * ceil(max(x) / bandwidth) + bandwidth / 2 + kde_step,
                                  kde_step))  # Generates the x values of the kde curve

    traces = [go.Histogram(x=x, name="bins", histnorm='percent', xbins=x_bins, marker=bar_marker,
                           hovertemplate='<b>bin</b>: %{x}' + unit + '<br><b>p.c.</b>: %{y:.2f}%<br>'),
              go.Scatter(x=values, y=np.exp(model.score_samples(values.reshape((len(values), 1)))) * 100, name="kde",
                         marker=line_marker,
                         hovertemplate='<b>val</b>: %{x}' + unit + '<br><b>p.c.</b>: %{y:.2f}%<br>')]
    if position is None:
        fig.add_traces(traces)
    else:
        fig.add_trace(traces[0], row=position[0], col=position[1])
        fig.add_trace(traces[1], row=position[0], col=position[1])
    return fig


def save_graph(fig, name, path='output/imgs', height=850, show=False, overwrite=False, transparency=False):
    """ Save a Plotly graph locally

    @param fig: Plotly Figure
    @param name: String
        Filename for the graph
    @param path: string
        Path to the saving directory
    @param height: Int
        Graph height
    @param show: boolean
        Display the graph in the user's interface
    @param overwrite: boolean
        Overwrite local file
    @param transparency: boolean
        Use a transparent background

    @author Thibault de Courson, 2022
    """

    fig.update_layout(height=height, hoverlabel=dict(namelength=-1))
    if transparency:
        fig.update_layout(paper_bgcolor='rgba(0,0,0,0)', plot_bgcolor='rgba(0,0,0,0)')
        name += '_transp'

    data_size = sum([np.sum(~pd.isna(i.y)) if i.y is not None and len(i.y) > 10 else 0 for i in fig.data])

    initialize_dir(path, overwrite=False)

    if overwrite or not isfile(path + '/' + name + '.png'):
        if data_size < 22 * (10 ** 5):
            fig.write_image(path + '/' + name + '.png', format='png', engine='kaleido', width=1920)
        else:
            print("ERROR: data to heavy to save:", data_size)
    if overwrite or not isfile(path + name + '.html'):
        if data_size < 8 * (10 ** 5):
            fig.write_html(path + '/' + name + '.html')
        else:
            print("ERROR: data to heavy to save as html file:", data_size)
    if show and data_size < 8 * (10 ** 5):
        fig.show()

def signal_overlay_graph(
        df,
        occurrences,
        transparency=False,
        delay_before=dt.timedelta(seconds=0),
        delay_after=dt.timedelta(seconds=0),
        y_label=None,
        mode='lines',
        indicators=('mean',),
        dark_mode=False,
        name='superposed'
):
    n_var = len(df.columns)

    # Initialize the graph
    fig = make_subplots(rows=n_var, cols=1, shared_xaxes=True, vertical_spacing=0.06, subplot_titles=df.columns)

    orientation, value = False, False
    if 'orientation' in occurrences.columns:
        orientation = True
    if 'value' in occurrences.columns:
        value = True

    this_color = DEFAULT_PLOTLY_COLORS[0]
    if transparency:
        opacity = 0.05
        this_color = "White" if dark_mode else "Black"
    else:
        opacity = 1

    occurrences_vals = []
    categorical = occurrences.value.dtype not in (int, float)
    cat_colors = occurrences.value.unique()
    cat_colors = dict(zip(cat_colors, DEFAULT_PLOTLY_COLORS[:len(cat_colors)]))

    # Add the lines
    for index, occurrence in occurrences.iterrows():

        # Format the data
        t_start = occurrence['start']
        t_min, t_max = t_start - delay_before, occurrence['end'] + delay_after
        vals = (
                df.loc[t_min:t_max, df.columns]
                .dropna(how='all')
                .dropna(axis=1, how='all')
                * (occurrence['orientation'] if orientation else 1))
        vals.index = [round(float(str(v - t_min)[13:]), 3) for v in vals.index]
        occurrences_vals.append(vals)

        # Prepare the hover template
        text_hover = "<b>ID</b>: " + str(index) + "<br><b>time</b>: " + str(t_start) + "<br>"
        if value:
            if not categorical and isnan(occurrence['value']):
                text_hover += "<b>no value</b>"
            else:
                text_hover += "<b>value</b>: " + str(occurrence['value'])

        # Choose the color
        if not transparency and value:
            if categorical:
                this_color = cat_colors[occurrence['value']]
            else:
                this_color = (
                    "yellow"
                    if isnan(occurrence['value'])
                    else "hsv(" + str(occurrence['value']) + ",100%,100%)"
                )

        # Add the lines to the graph
        for i_line in range(n_var):
            fig.add_trace(
                go.Scatter(x=vals.index, y=vals[df.columns[i_line]],
                           mode=mode, connectgaps=False, line=dict(width=1, color=this_color), opacity=opacity,
                           hovertemplate="<b>t</b>: %{x}s<br><b>value</b>: %{y:.1f}<br><extra>" + text_hover + "<br></extra>",
                           ),
                row=i_line + 1,
                col=1,
            )

    # Add indicators
    if not transparency and len(indicators) > 0:
        all_index = pd.Index(
            sorted(set().union(*(df.index for df in occurrences_vals))))  # List all possible index values

        # Reindex all dataframes to have the same index, convert to a 3D array & calculate the median along the 3rd axis
        occurrences_vals = np.stack([df.reindex(index=all_index).values for df in occurrences_vals])

        # Add the medians to the graph
        for i in range(occurrences_vals.shape[2]):
            if 'median' in indicators:
                fig.add_trace(go.Scatter(x=all_index, y=np.nanmedian(occurrences_vals[:, :, i], axis=0),
                                         connectgaps=True, mode='lines',
                                         line=dict(width=2, color='white' if dark_mode else 'black'),
                                         hovertemplate='<b>t</b>: %{x}s<br><b>median</b>: %{y:.1f}°<br>'), row=i + 1,
                              col=1)
            if 'mean' in indicators or 'std' in indicators:
                mean = np.nanmean(occurrences_vals[:, :, i], axis=0)
                if 'mean' in indicators:
                    fig.add_trace(go.Scatter(x=all_index, y=mean,
                                             connectgaps=True, mode='lines',
                                             line=dict(width=2, color='white' if dark_mode else 'black'),
                                             hovertemplate='<b>t</b>: %{x}s<br><b>mean</b>: %{y:.1f}°<br>'), row=i + 1,
                                  col=1)
                if 'std' in indicators:
                    std = np.nanstd(occurrences_vals[:, :, i], axis=0)
                    fig.add_trace(go.Scatter(x=all_index, y=mean + std,
                                             connectgaps=True, mode='lines',
                                             line=dict(width=2, color='white' if dark_mode else 'black'),
                                             hovertemplate='<b>t</b>: %{x}s<br><b>std sup</b>: %{y:.1f}°<br>'),
                                  row=i + 1, col=1)
                    fig.add_trace(go.Scatter(x=all_index, y=mean - std,
                                             connectgaps=True, mode='lines',
                                             line=dict(width=2, color='white' if dark_mode else 'black'),
                                             hovertemplate='<b>t</b>: %{x}s<br><b>std inf</b>: %{y:.1f}°<br>'),
                                  row=i + 1, col=1)
        # if len(speeds) > 0:
        #     fig.add_trace(go.Scatter(x=[None], y=[None], mode='markers', hoverinfo='none',
        #                                 marker=dict(colorscale=[(0,'hsv(180,100%,100%)'), (0.5,'hsv(270,100%,100%)'), (1,'hsv(360,100%,100%)')],
        #                                             showscale=True, cmin=min_speed, cmax=max_speed,
        #                                             colorbar=dict(thickness=10, ticks='outside', title=dict(text='Wind speed (m/s)', side='right'))
        #                                 )))

    # Graph layout
    fig.update_xaxes(title_text="Time from the beginning of the occurrence (s)", row=n_var, col=1)
    if y_label is not None:
        fig.update_yaxes(title_text=y_label, row=round(n_var / 2), col=1)
    fig.update_traces(showlegend=False)

    if dark_mode:
        fig.update_layout(template='plotly_dark')
    fig.update_layout(
        height=850, hoverlabel=dict(namelength=-1)
    )
    save_graph(fig, name=name, overwrite=True, show=True)
