import numpy as np
from tensorflow import keras  # for building Neural Networks
import keras_tuner as kt
# for creating regular densely-connected NN layers
from tensorflow.keras.layers import Dense, Input, GaussianNoise, Dropout
from tensorflow.keras import Sequential
from tensorflow.keras.initializers import HeNormal, Constant
from sklearn.model_selection import KFold


# Overwrite run trial to tune batch size and to apply cross validation
class MyTuner(kt.RandomSearch):
    def run_trial(self, trial, x, y, epochs, batch_size, callbacks=None, *args, **kwargs):
        cv = KFold(5)
        val_losses = []
        for train_indices, test_indices in cv.split(x):
            x_test, y_test = x.iloc[test_indices], y.iloc[test_indices]

            model = self.hypermodel.build(trial.hyperparameters)
            print(x.iloc[train_indices].shape, y.iloc[train_indices].shape, type((x_test, y_test)), type(batch_size),
                  type(epochs), type(callbacks))

            model.fit(x.iloc[train_indices], y.iloc[train_indices], validation_data=(x_test, y_test),
                      batch_size=batch_size, epochs=epochs, callbacks=callbacks)
            model.fit(x.iloc[train_indices], y.iloc[train_indices], validation_data=(x_test, y_test),
                      batch_size=batch_size, epochs=epochs, callbacks=callbacks)

            val_losses.append(model.evaluate(x_test, y_test, return_dict=True)['loss'])

        self.oracle.update_trial(trial.trial_id, {'val_loss': np.mean(val_losses)})
        # self.save_model(trial.trial_id, model)


# model construction for training
def build_model(input_shape, num_layers=3, units=[20, 20, 20, 20, 20, 20, 20], learning_rate=0.01, activation='relu',
                dropout=False, droprates=[0.2, 0.2, 0.2, 0.2, 0.2, 0.2, 0.2], noise_dev=None):
    initializer = HeNormal()
    bias_initializer = Constant(0.2)

    model = Sequential()

    # Data augmentation layer if wanted
    if noise_dev is not None:
        model.add(GaussianNoise(stddev=noise_dev, input_dim=input_shape))
        model.add(
            Dense(units[0], activation=activation, kernel_initializer=initializer, bias_initializer=bias_initializer))
    else:
        model.add(Dense(units[0], activation=activation, input_dim=input_shape, kernel_initializer=initializer,
                        bias_initializer=bias_initializer))

    # Add layers in for loop to be able to tune number of layers
    for i in range(1, num_layers):
        model.add(
            Dense(units[i], activation=activation, kernel_initializer=initializer, bias_initializer=bias_initializer))
        if dropout and len(droprates) > i:
            model.add(Dropout(droprates[i]))

    model.add(Dense(1, activation='sigmoid'))

    model.compile(optimizer=keras.optimizers.Adam(learning_rate=learning_rate),
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    return model


# Hyperparameter tuning
def build_tuning_model(hp, input_shape, n_layers=(1, 4), n_units=(8, 64), activation=('relu', 'elu'),
                       learning_rate=(.0001, .1), noise=None):
    # Initializer
    initializer = HeNormal()
    bias_initializer = Constant(0.2)

    # Set Hyperparameters
    learning_rate = hp.Float('learning_rate', min_value=learning_rate[0], max_value=learning_rate[1], sampling='log')
    activation = activation if type(activation) == str else hp.Choice('activation', activation)
    num_layers = hp.Int('num_layers', min_value=n_layers[0], max_value=n_layers[1], step=1)

    model = Sequential()

    # Data augmentation layer if wanted
    if noise is not None:
        model.add(
            GaussianNoise(stddev=hp.Float('stddev', min_value=noise[0], max_value=noise[1]), input_dim=input_shape))

    model.add(Dense(hp.Int('units_0', min_value=n_units[0], max_value=n_units[1], step=1),
                    activation=activation, kernel_initializer=initializer, bias_initializer=bias_initializer))
    # model.add(Dropout(hp.Float('droprate_0', min_value=0, max_value=0.6)))

    for i in range(1, num_layers):
        model.add(Dense(hp.Int('units_' + str(i + 1), min_value=n_units[0], max_value=n_units[1], step=1),
                        activation=activation, kernel_initializer=initializer, bias_initializer=bias_initializer))
        # model.add(Dropout(hp.Float('droprate_'+str(i), min_value=0, max_value=0.6)))

    model.add(Dense(1, activation='sigmoid'))

    model.compile(optimizer=keras.optimizers.Adam(learning_rate=learning_rate),
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    return model
