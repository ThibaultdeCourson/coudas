import pytest
import datetime as dt

from src.couDAS import octet_rectifier
from src.couDAS.engineering import measure_duration

def test_octet_rectifier():
    assert octet_rectifier(-10) == 0
    assert octet_rectifier(260) == 255
    assert octet_rectifier(200.6) == 201

def test_measure_duration(capfd):
    t1 = dt.datetime.now()
    t2 = t1 + dt.timedelta(seconds=42)
    measure_duration(t1, t2)
    out, err = capfd.readouterr()
    assert out == "42.0 sec\n"